import numpy as np
import matplotlib as mp
import pandas as p
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from math import e
import math as mt
import pickle as pl

import datetime as dt
from datetime import timedelta


import sys
import csv
import os,time



sys.path.append('../commons')

#p.set_option('display.max_rows', None)
#p.set_option('display.max_columns', None)
#p.set_option('mode.chained_assignment', None)
p.options.mode.chained_assignment = None  # default='warn'

def logistic(t,L,k,t0):
	return L / (1. + np.exp(-k*(t-t0)) )

# A is t-shifting parameter. B is relaxation time. e^C is total infected at saturation
def gompertz(t,A,B,C):
	return np.exp(C-A*np.exp(-t/B))

def fit_logistic(data_name,data_in,param0):
	print('fitting '+data_name+' with logistic initial parameters ',param0)
	n =len(data_in)
	t = np.linspace(0,n-1,n)
	y = data_in
	par,cov = curve_fit(logistic,t,y,param0,method='lm')
	return par,cov

def fit_gompertz(data_name,data_in,param0):
	print('fitting '+data_name+' with gompertz initial parameters ',param0)
	n =len(data_in)
	t = np.linspace(0,n-1,n)
	y = data_in
	par,cov = curve_fit(gompertz,t,y,param0,method='lm')
	return par,cov


starting_date =  dt.date(2020,2,24)

#update automatico dei dati
import covid_datadownloader
covid_datadownloader.checkDatasetFreshness()

data = p.read_csv("../commons/dataItaly.csv")

#nuovi campi derivati (differenze giorno per giorno)
derivate=[
#"totale_casi", rimosso per inserimento nel dataset dei nuovi_casi alla sorgente
"tamponi",
"deceduti",
"casi_testati"
]

for n in derivate:
	data["puntuale_"+n] = np.nan
	data["puntuale_"+n] = data["puntuale_"+n].fillna(data[n].diff())# + data["puntuale_casi"]
	
	#print(data["puntuale_"+n])

	for i,row in data[data["puntuale_"+n].isnull()].iterrows():
		#data.at[i,"puntuale_"+n] = 0
		data["puntuale_"+n][i] = 0
		

# calcolo del rapporto tra nuovi casi e tamponi eseguiti

data["rateo_tamponi_positivi"] = np.nan
#data["rateo_tamponi_positivi"] = data["rateo_tamponi_positivi"].fillna( (data["nuovi_positivi"]/data["puntuale_tamponi"])*100 )
data["rateo_tamponi_positivi"] = data["rateo_tamponi_positivi"].fillna( (data["nuovi_positivi"]/data["puntuale_casi_testati"])*1 )

data["rateo_tamponi_positivi"][0]=0

#print(data["rateo_tamponi_positivi"])

# plot
plotting = 'y'
plotCounter = 0;

while plotting == 'y':
	
	plotCounter = plotCounter + 1 
	# interfaccia per la richiesta del plot
	print('choose one of the following dataset to fit:')
	kys = list(data.keys())
	for i,v in enumerate(kys):
		print(i,v)
	what_fit_n = input('enter the number (default totale_casi): ') or '13'
	
	what_fit_list = [int(x) for x in what_fit_n.split(',')]
	print(what_fit_list)
	
	fitting_methods = ['none','logistic','gompertz']
	print('\nchoose the fitting method')
	for i,v in enumerate(fitting_methods):
		print(i,v)
	curve_n = input('enter the number (default none): ') or 0
	
	
	fig = plt.figure()
	for what_to_fit in what_fit_list:
	
	
		what_fit = kys[int(what_to_fit)]
		curve = fitting_methods[int(curve_n)]
		if curve == 'gompertz':
			print('fitting to gompertz: f =exp( C - A*exp(-t/B) )\n')
		elif curve == 'logistic':
			print('fitting to logistic: f = L / ( 1 + exp( -k*(t-t0) ) )\n')
			
	
	
	
		#data_to_fit = [int(i) for i in data[what_fit]]
		data_to_fit = [i for i in data[what_fit]]
	
	
	
		par0_lgs = [40000,0.3,20] # L,K,t0
		par0_gmp = [50,5,10] # A,B,C
	
	
			
	
		if curve == 'logistic':
			par,cov = fit_logistic(what_fit,data_to_fit,par0_lgs)
			print('fitted parameters: L={:.0f} k={:.2f} t0={:.0f}'.format(par[0],par[1],par[2]))
			n =len(data_to_fit)
			t_fit = np.linspace(0,1.5*n,10*n)
			y_fit = logistic(t_fit,par[0],par[1],par[2])
	
	
		elif curve == 'gompertz':
			par,cov = fit_gompertz(what_fit,data_to_fit,par0_gmp)
			print('fitted parameters: A={:.2f} B={:.2f} C={:.2f}'.format(par[0],par[1],par[2]))
			n =len(data_to_fit)
			t_fit = np.linspace(0,1.5*n,10*n)
			y_fit = gompertz(t_fit,par[0],par[1],par[2])
			
		elif curve == 'none':
			n =len(data_to_fit)
	
		t = np.linspace(0,n-1,n)
		t =[starting_date + dt.timedelta(days=dd) for dd in t ]
	
	
		#fig.suptitle(what_fit)
	
		if curve != 'none':
			plt.plot(t_fit,y_fit)
	
		plt.plot(t,data_to_fit,label=what_fit)
		plt.xlabel('days from feb 24')
		plt.ylabel(what_fit)
		plt.legend()
	
	plt.ion()
	plt.show()
	
	plotting = input("plot something else? y/n ")
#plt.show()

