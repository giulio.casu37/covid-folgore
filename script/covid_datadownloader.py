import os
import requests
import pathlib
import datetime as dt
datapath= "../commons"
urls = [
    ("dataRegioni.csv","https://raw.githubusercontent.com/pcm-dpc/COVID-19/master/dati-regioni/dpc-covid19-ita-regioni.csv"),
    ("dataItaly.csv","https://raw.githubusercontent.com/pcm-dpc/COVID-19/master/dati-andamento-nazionale/dpc-covid19-ita-andamento-nazionale.csv")
]
datafolder= os.path.join(os.path.dirname(__file__), datapath)
pathlib.Path(datafolder).mkdir(parents=True, exist_ok=True)

def checkDatasetFreshness():
    try:
        f = open ("../commons/lastDataUpdate","r")
        lus=f.readline()
        f.close()
        lastUpdate = dt.datetime.fromisoformat(lus)
    except Exception as e:
        print(e)
        # something fucked up, usiamo una data antica
        lastUpdate= dt.datetime(1950,5,10)
                
    updateDelta = dt.datetime.now() - lastUpdate
    print(f"Last data update was {round(updateDelta.total_seconds() / (3600), 2)} hours ago")
    
    if(updateDelta.total_seconds() / (3600) < 24):
        print("Data still fresh!")
    else:
        print("Updating...")
        updateDataset()
        f = open("../commons/lastDataUpdate","w")
        f.write(dt.datetime.isoformat(dt.datetime.now()))
        f.close()
        print ("Data updated!")

def updateDataset():
    for url in urls:
        print(f"scaricando {url[0]}, da {url[1]}")
        with open(os.path.join(datafolder, url[0]), "wb") as downloadedfile:
            downloadedfile.write(requests.get(url[1]).content)
            print("scaricato!")

    