import numpy as np
import matplotlib as mp
import pandas as p
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from math import e
import math as mt
import pickle as pl

from scipy.signal import savgol_filter
import datetime as dt
from datetime import timedelta



import sys
import csv
import os,time

sys.path.append('../commons')

p.set_option('display.max_rows', None)
#p.set_option('display.max_columns', None)
#p.set_option('mode.chained_assignment', None)
p.options.mode.chained_assignment = None  # default='warn'

def logistic(t,L,k,t0):
	return L / (1. + np.exp(-k*(t-t0)) )

# A is t-shifting parameter. B is relaxation time. e^C is total infected at saturation
def gompertz(t,A,B,C):
	return np.exp(C-A*np.exp(-t/B))

def fit_logistic(data_name,data_in,param0):
	print('fitting '+data_name+' with logistic initial parameters ',param0)
	n =len(data_in)
	t = np.linspace(0,n-1,n)
	y = data_in
	par,cov = curve_fit(logistic,t,y,param0,method='lm')
	return par,cov

def fit_gompertz(data_name,data_in,param0):
	print('fitting '+data_name+' with gompertz initial parameters ',param0)
	n =len(data_in)
	t = np.linspace(0,n-1,n)
	y = data_in
	par,cov = curve_fit(gompertz,t,y,param0,method='lm')
	return par,cov
	

starting_date =  dt.date(2020,2,24)

#Update automatico dei dati
import covid_datadownloader
covid_datadownloader.checkDatasetFreshness()

data = p.read_csv("../commons/dataRegioni.csv")
data = data.sort_values('data')

#dic_abitanti2 = p.read_csv("../commons/regioni_abitanti_pandas.csv")

dic_abitanti = {}
with open('../commons/regioni_abitanti') as inputfile:
    for row in csv.reader(inputfile):
        dic_abitanti[row[0]] = int(row[1])

#nuovi campi derivati (differenze giorno per giorno)
derivate=[
#"totale_casi", rimosso per inserimento nel dataset dei nuovi_casi alla sorgente
"tamponi",
"deceduti",
"casi_testati"
]

for n in derivate:
	data["puntuale_"+n] = np.nan
	for i in data['denominazione_regione'].unique():
		data["puntuale_"+n] = data["puntuale_"+n].fillna(data[data['denominazione_regione']==i][n].diff())# + data["puntuale_casi"]
		

	for i,row in data[data["puntuale_"+n].isnull()].iterrows():
		#data.at[i,"puntuale_"+n] = 0
		data["puntuale_"+n][i] = 0



#aggiungo il rateo tamponi positivi

data["rateo_tamponi_positivi"] = np.nan

for i in data['denominazione_regione'].unique():
	data["rateo_tamponi_positivi"] = data["rateo_tamponi_positivi"].fillna( (data[data['denominazione_regione']==i]["nuovi_positivi"]/data[data['denominazione_regione']==i]["puntuale_casi_testati"]) )

	data[data['denominazione_regione']==i]["rateo_tamponi_positivi"][0]=0

#Rimuovo i valori NaN
for i,row in data[~np.isfinite(data["rateo_tamponi_positivi"])].iterrows():
		#data.at[i,"puntuale_"+n] = 0
		data["rateo_tamponi_positivi"][i] = 0


#print(data['puntuale_totale_casi'][data['puntuale_totale_casi'].isnull()])


#data["puntuale_casi"] = np.nan
#data["puntuale_tamponi"] = np.nan
#for i in data['denominazione_regione'].unique():
	#data["puntuale_casi"] = data["puntuale_casi"].fillna(data[data['denominazione_regione']==i]["totale_casi"].diff())# + data["puntuale_casi"]
	#data["puntuale_tamponi"] = data["puntuale_tamponi"].fillna(data[data['denominazione_regione']==i]["tamponi"].diff())# + data["puntuale_casi"]
	#print(i)
	#print(data[data['denominazione_regione']==i][["totale_casi","puntuale_casi"]])

#print (data[data['denominazione_regione']=='Lombardia'][["totale_casi","puntuale_casi"]])

plotting = 'y'
plotCounter = 0

while plotting == 'y':
	
	plotCounter = plotCounter + 1 

	print('choose one of the following dataset to fit:')
	kys = list(data.keys())
	for i,v in enumerate(kys):
		print(i,v)
	what_fit_n = input('enter the number/s separated by comma (default totale_casi): ') or '15'
	
	what_fit_list = [int(x) for x in what_fit_n.split(',')]
	
	print('\nchoose one of the following regions:')
	regioni = []
	for i in data['denominazione_regione']:
		if i not in regioni:
			regioni.append(i)
	for i,v in enumerate(regioni):
		print(i,v)
	what_fit_reg_n = input('enter the number/s separated by comma (default Sardegna): ') or '10'
	
	what_fit_reg_list = [int(x) for x in what_fit_reg_n.split(',')]
	what_fit_reg = [regioni[x] for x in what_fit_reg_list]
	
	
	
	fitting_methods = ['none','logistic','gompertz']
	print('\nchoose the fitting method')
	for i,v in enumerate(fitting_methods):
		print(i,v)
	curve_n = input('enter the number/s separated by comma (default none): ') or 0
	
	normalization = ['no','yes']
	print('\nchoose wether to normalize by inhabitants or not')
	for i,v in enumerate(normalization):
		print(i,v)
	normaliz = input('enter the number/s separated by comma (default no): ') or 0
	
	print()
	smoothing = ['no','yes']
	print('do you want to smoothe the curve?')
	for i,v in enumerate(smoothing):
		print(i,v)
	smooth = input('enter the number, must be odd (default = 1)') or 1
	smooth = int(smooth)
	if int(smooth) == 1:
		point_to_clump = input('how many days togheter? (default 31) has to be odd:') or 31
	
	
	for what_to_fit in what_fit_list:
		fig = plt.figure()
		what_fit = kys[int(what_to_fit)]
		if normaliz:
			fig.suptitle(what_fit+' ogni 10^6 abitanti')
		else:
			fig.suptitle(what_fit)
		
		for regione in what_fit_reg:
	
	
			curve = fitting_methods[int(curve_n)]
			if curve == 'gompertz':
				print('fitting to gompertz: f =exp( C - A*exp(-t/B) )\n')
			elif curve == 'logistic':
				print('fitting to logistic: f = L / ( 1 + exp( -k*(t-t0) ) )\n')
				
	
			if normaliz:
				data_to_fit = []
				for i,v in enumerate(data[what_fit]):
					if list(data['denominazione_regione'])[i] == regione:
						data_to_fit.append(int(v)/dic_abitanti[regione])
			
			else:
				data_to_fit = []
				for i,v in enumerate(data[what_fit]):
					if list(data['denominazione_regione'])[i] == regione:
						data_to_fit.append(v)
	
	
	
	
			par0_lgs = [40000,0.3,20] # L,K,t0
			par0_gmp = [50,5,10] # A,B,C
	
	
				
	
			if curve == 'logistic':
				par,cov = fit_logistic(what_fit,data_to_fit,par0_lgs)
				print('fitted parameters: L={:.0f} k={:.2f} t0={:.0f}'.format(par[0],par[1],par[2]))
				n =len(data_to_fit)
				t_fit = np.linspace(0,1.5*n,10*n)
				y_fit = logistic(t_fit,par[0],par[1],par[2])
	
	
			elif curve == 'gompertz':
				par,cov = fit_gompertz(what_fit,data_to_fit,par0_gmp)
				print('fitted parameters: A={:.2f} B={:.2f} C={:.2f}'.format(par[0],par[1],par[2]))
				n =len(data_to_fit)
				t_fit = np.linspace(0,1.5*n,10*n)
				y_fit = gompertz(t_fit,par[0],par[1],par[2])
				
			elif curve == 'none':
				n =len(data_to_fit)
	
			t = np.linspace(0,n-1,n)
			t =[starting_date + dt.timedelta(days=dd) for dd in t ]
	
	
			
			if curve != 'none':
				plt.plot(t_fit,y_fit)

			if int(smooth) == 1:
				data_to_fit = savgol_filter(data_to_fit, int(point_to_clump), 3)
	
			plt.plot(t,data_to_fit,label=regione)
			plt.xlabel('days from feb 24')
			plt.ylabel(what_fit)
			plt.legend()
	
	plt.ion()
	plt.show()
	
	plotting = input("plot something else? y/n ")











