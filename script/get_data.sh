
mv ../commons/dataRegioni.csv ../commons/dataRegioni_backup.csv
mv ../commons/dataItaly.csv ../commons/dataPC_backup.csv

wget -O ../commons/dataRegioni.csv https://raw.githubusercontent.com/pcm-dpc/COVID-19/master/dati-regioni/dpc-covid19-ita-regioni.csv

wget -O ../commons/dataItaly.csv https://raw.githubusercontent.com/pcm-dpc/COVID-19/master/dati-andamento-nazionale/dpc-covid19-ita-andamento-nazionale.csv
