# COVID FOLGORE PLOTTER

simple plotting tool for covid data in italy. Data is given from "protezione civile", and can be plotted by region or total national. software input and output is in italian language. if you need translation please contact me.

## DISCLAIMER
This program is provide open-source, for free, as-is, and with no warranty. 
The authors waive any responsibility for any damage that may result from its use in any context.

## AUTHORS
Giulio Casu, Anselmo Casu, Luigi E. A. Serra.

Together known as folgore

## REQUISITES
Python 3.7 or superior

The following Python packages are needed:
- Numpy
- Scipy 
- Matplotlib
- Pandas
- Requests

## STRUCTURE
there are 3 main scripts in "script" folder:
- covid_datadownloader.py : python script that fetches the latest data provided from "protezione civile"
- covid_plot.py : python3 script that plots national quantities (many datas can be plotted in the same graph)
- covid_plot_reg.py : python3 script that plots regional quantities (many region for the same data can be plotted in the same graph)

the commons folder contains the databases, in a typical usage you should not operate on this folder.

## HOW TO USE THE CODE
go inside "script" folder. then run either
- python3 covid_plot.py

or 
- python3 covid_plot_reg.py

and follow the prompted output. In first place the software will ask if you want to automatically update datasets, in case that the dataset is at least 1 day old. Then proper plot input is required.
You can plot togheter many quantities and many regions, so that you can compare them; if you wish to do so just type the quantities separated by commas, e.g. 10,12,15 and then press enter.

At the end the prompt will ask if you wish to plot somenthing else or close the program. 




